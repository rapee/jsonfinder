# JSON Finder

Browse JSON like you do it in Finder.

* View like you browse file system.
* Good for large JSON: column view helps you keep track of deep object or array.
* Find in JSON and jump to matches. (Press ⌘+F on OSX)
* Mouse or arrow keys to navigate.
* Support browser back/forward.

# Installation

Install from [Chrom Web Store here](https://www.google.co.jp/url?sa=t&rct=j&q=&esrc=s&source=web&cd=5&cad=rja&ved=0CE8QFjAE&url=https%3A%2F%2Fchrome.google.com%2Fwebstore%2Fdetail%2Fjson-finder%2Fflhdcaebggmmpnnaljiajhihdfconkbj%3Fhl%3Den&ei=_coZUfLNLoSEkgXHw4HQAQ&usg=AFQjCNG-zm83VZeuSmSP_4D2QuA-OYkPJg&sig2=LJBlfEycUhykYqTQ5vrSYw&bvm=bv.42261806,d.dGI).

# Sample JSON

* [http://earthquake.usgs.gov/earthquakes/feed/geojson/2.5/day](http://earthquake.usgs.gov/earthquakes/feed/geojson/2.5/day)

# Change Log

0.0.6

* NEW: Show selected key path
* NEW: Display array length
* NEW: Make column resizable

0.0.5

* FIX: null bug

0.0.4

* NEW: Search key and value in JSON
* NEW: Support history and hash change
* FIX: CSS encoding bug

0.0.3

* FIX: CSS injection bug
